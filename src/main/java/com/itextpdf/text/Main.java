package com.itextpdf.text;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;

public class Main {
    public static void main(String[] args) {
        Document document = new Document();

        try {
            PdfWriter.getInstance(document,
                    new FileOutputStream("Resume.pdf"));

            document.open();
            PdfPTable table = new PdfPTable(2); // 2 columns.
            var font = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD);
            var header = new Paragraph("Resume",font);
            header.setAlignment(Element.ALIGN_CENTER);
            PdfPCell cell1 = new PdfPCell(new Paragraph("First name"));
            PdfPCell cell2 = new PdfPCell(new Paragraph("Sebastian"));
            PdfPCell cell3 = new PdfPCell(new Paragraph("Last name"));
            PdfPCell cell4 = new PdfPCell(new Paragraph("Ziobron"));
            PdfPCell cell5 = new PdfPCell(new Paragraph("Profession"));
            PdfPCell cell6 = new PdfPCell(new Paragraph("Student"));
            PdfPCell cell7 = new PdfPCell(new Paragraph("Education"));
            PdfPCell cell8 = new PdfPCell(new Paragraph( "2017 - PWSZ Tarnów \n"+
                    "2013-2017 - Technikum"));
            PdfPCell cell9 = new PdfPCell(new Paragraph("Summary"));
            PdfPCell cell10 = new PdfPCell(new Paragraph("Brak"));

            table.addCell(cell1);
            table.addCell(cell2);
            table.addCell(cell3);
            table.addCell(cell4);
            table.addCell(cell5);
            table.addCell(cell6);
            table.addCell(cell7);
            table.addCell(cell8);
            table.addCell(cell9);
            table.addCell(cell10);


            header.add(table);
            document.add(header);

            document.close();
            System.out.println("yes");
        } catch(Exception e){
            System.out.println("no");
        }
    }

}


